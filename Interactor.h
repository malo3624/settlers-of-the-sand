// Interactor.h
// Interactor class - meta class for player, AI, bank, and robber.
#ifndef INTERACTOR_H
#define INTERACTOR_H
#include "Board.h"
#include <string>
#include <vector>

class Interactor
{
public:
    Interactor();
    ~Interactor();
    
    // Trading functions, with paramter of other player.
    void Build(Board*);   // interact with board
    
    std::string GetName() {  return name_; }
    int GetScore() { return score_; }

    // inventory_[0] = water, inventory_[1] = metal, inventory_[2] = oil, inventory_[4] = livestock
    int inventory_[4];
    
private:
    // Player, AI - has access to deck, inventory of resources, tracks roads and cities it controls;
    //                        must manage between inventory and towns (follow basic strats?) seek inventory and city/road goals.
    
    int score_;
    std::string name_;
};

class Player : public Interactor
{
public:
    Player();
    int Points();
    
    //adds one each time a town is constructed
    void TIncrement() {town_count += 1;}
    //adds one each time a road is constructed
    void RIncrement() {road_count += 1;}
    
    //returns number of roads
    int GetNumRoads() const {return road_count;}
    //returns numeber of towns
    int GetNumTowns() const {return town_count;}
    
private:
    int town_count;
    int road_count;
    int points_;
    unsigned int resources[4];
};

class AI : public Player
{
    /*
     - AI trading behaviour - Main goal is to have equal amounts of every material, i.e. inventory_[0] ~= inventory[1]_, give or take some 'n'.
     - However, should inventory be even, then it should focus on building materials, like metal and oil.
     */
public:
    AI();
};
#endif
