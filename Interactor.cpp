// Source file for Interactor.h
#include "Interactor.h"

// Interactor meta class functions
Interactor::Interactor()
{
    for(int i =0; i < 4; i++){
        inventory_[i] = 0;
    }
}

// Destructor
Interactor::~Interactor()
{
    ;
}


// Interact with board to add roads and towns
void Interactor::Build(Board* player_board)   // interact with board
{
    // Options: build road or town?
    // Let player choose location
    // Verify two things:
        // 1) Player has enough resources to build
        // 2) Location is adjacent to point owned by another player.
    std::cout << "Testing" << std::endl;
}

// A function to calculate player's score based on inventory and board holdings.
    // Point system:
    // 1 point for every road, 2 points for every town
    // Goal is to get to 5 pts.
    // May have to call on board.
// Player class functions
Player::Player()
{
    for (int i = 0; i < 4; i++)
        resources[i] = 0;
}

//counts the points for each player
int Player::Points()
{
    points_ = (town_count * 2) + road_count;
    return points_;
}




// AI class functions
AI::AI()
{
    ;
}
