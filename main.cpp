// Connor Dowd, Victoria Velasquez, Matthew Louchart

//  main.cpp
//

#include "Bank.h"
#include "Game.h"
#include "Board.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <string>
//#include <ncurses.h>


int main(int argc, const char * argv[]) {
    std::srand(time(NULL));
    
   // initscr();
    int num_players;
    Tile test;
    std::string elements[4] = {"Water", "Metal", "Oil", "Livestock"};
    
    // This is the opening page to the game giving an introduction to the user.
    std::cout << "WELCOME TO SETTLERS OF SAND!\n";
    std::cout << "____________________________\n\n";
    std::cout << "Try to get to 5 points to win the game!\n";
    std::cout << "Open settlements locations are indicated with: #\n";
    std::cout << "Your settlements are indicated with: $\n";
    std::cout << "Open road locations are indicated with: |\n";
    std::cout << "Your roads are indicated with: ! or ._.\n";
    std::cout << "Build a road = 1 point\nBuild a settlement = 2 points\n\n";
    std::cout << "To build a road you need: 1 oil and 1 metal\n";
    std::cout << "To build a settlement you need: 1 oil, 1 metal, 1 water, 1 livestock\n\n";
    
    // Prompts the user to choose how many players they would like to have in the game.
    printf ("How many players would you like? (choose 1): ");
    scanf ("%d", &num_players);
    
    //Would have had multiple players, however this functionality was not fully implemented. (See bellow)
    
    /* This is if the player chooses an invalid number.
     It then gives the prompt of how many players again.
     */
    /*while (num_players > 4 || num_players < 1) {
        //std::cout <<"How many players would you like? (choose 1-4): ";
        //std::cin  >> num_players ;
        
        printf ("\nNot a valid number of players, please choose again.\n");
        printf ("How many players would you like? (choose 1-4): ");
        scanf ("%d", &num_players);
    }*/
    
    while (num_players > 1 || num_players < 1) {
        //std::cout <<"How many players would you like? (choose 1-4): ";
        //std::cin  >> num_players ;
        
        printf ("\nNot a valid number of players, please choose again.\n");
        printf ("How many players would you like? (choose 1-4): ");
        scanf ("%d", &num_players);
    }
    
    
    // Displays the number of players in the game and the symbol that goes with each player.
    if (num_players==1) {
        printf ("\nPlayer 1 Symbol: $\n");
    }
    
    if (num_players==2) {
        printf ("\nPlayer 1 Symbol: P1\n");
        printf ("Player 2 Symbol: P2\n");
    }
    
    if (num_players==3) {
        printf ("\nPlayer 1 Symbol: P1\n");
        printf ("Player 2 Symbol: P2\n");
        printf ("Player 3 Symbol: P3\n");
    }
    
    if (num_players==4) {
        printf ("\nPlayer 1 Symbol: P1\n");
        printf ("Player 2 Symbol: P2\n");
        printf ("Player 3 Symbol: P3\n");
        printf ("Player 4 Symbol: P4\n");
    }

    //std::vector<Player> game_players;
    Player p;

    /* Prompt given to the player to start the game.
     If the player chooses 0 the game will start,
     if the player chooses 1 the game will exit.
     */
    int begin_game;
    printf ("\nReady to start game? (type 0 for yes, type 1 for no): ");
    scanf ("%d", &begin_game);
    
    int row_coord;
    int col_coord;
    int _size = 9;
    Board b(_size);
    Bank bank;

    while (begin_game > 1) {
        printf ("\nNot a valid choice.\n");
        printf ("Ready to start game? (type 1 for yes, type 0 for no): ");
        scanf ("%d", &begin_game);
    }
    
    if (begin_game == 0) {
        printf ("\nBoard Symbols:\n");
        printf ("Livestock = L \nWater = W \nMetal = M \nOil = O\n");
        
        //Choose your starting setlement location by putting in the coordinates associated with the board.
        //Must choose a # for a settlement, if a different location is chosen it will ask again.
        printf ("\nPlayer 1 pick your starting settlement: \n\n");
        std::cout << b.GetBoard() << '\n';

        printf ("\n\nEnter a row coordinate: ");
        scanf ("%d", &row_coord);
        printf ("\nEnter a column coordinate: ");
        scanf ("%d", &col_coord);
        
        while (b.GetTile(col_coord, row_coord).getIcon() != "# ") {
            printf ("\nNot valid. You did not choose a settlement choose again.");
            printf ("\n\nEnter a row coordinate: ");
            scanf ("%d", &row_coord);
            printf ("\nEnter a column coordinate: ");
            scanf ("%d", &col_coord);
        }
        
        //Replaces # with player symbol of $ for their new settlement
        if (b.GetTile(col_coord, row_coord).getIcon() == "# ") {
            test=b.GetTile(col_coord, row_coord);
            test.setIcon("$ ");
            b.SetTile(col_coord, row_coord, test);
            b.MakeBoard();
            p.TIncrement();
            printf ("\n");
            std::cout << b.GetBoard() << '\n';
        }
    }
    
    else if (begin_game == 1) {
        return 0;
    }
    
    //Choose your starting road location by putting in the coordinates associated with the board.
    //Must choose a | or _ for a settlement, if a different location is chosen it will ask again.
    printf ("\nPlayer 1 pick your starting road: \n\n");
    
    printf ("\n\nEnter a row coordinate: ");
    scanf ("%d", &row_coord);
    printf ("\nEnter a column coordinate: ");
    scanf ("%d", &col_coord);
    
    while (b.GetTile(col_coord, row_coord).getIcon() != "| " & b.GetTile(col_coord, row_coord).getIcon() != "\u2014\u2014\u2014 ") {
        printf ("\nNot valid. You did not choose a road choose again.");
        printf ("\n\nEnter a row coordinate: ");
        scanf ("%d", &row_coord);
        printf ("\nEnter a column coordinate: ");
        scanf ("%d", &col_coord);
    }
    
    //Replaces open road with users road
    if (b.GetTile(col_coord, row_coord).getIcon() == "| ") {
        test=b.GetTile(col_coord, row_coord);
        test.setIcon("! ");
        b.SetTile(col_coord, row_coord, test);
        b.MakeBoard();
        p.RIncrement();
        printf ("\n");
        std::cout << b.GetBoard() << '\n';
    }
    else if (b.GetTile(col_coord, row_coord).getIcon() == "\u2014\u2014\u2014 ") {
        test=b.GetTile(col_coord, row_coord);
        test.setIcon("._. ");
        b.SetTile(col_coord, row_coord, test);
        b.MakeBoard();
        p.RIncrement();
        printf ("\n");
        std::cout << b.GetBoard() << '\n';
    }
    
    
    // Prompts the player to roll the dice.
    int ready_dice;
    printf ("\nready to roll dice? (type 0 for yes, type 1 for no): ");
    scanf ("%d", &ready_dice);
    
    while (ready_dice > 0) {
        printf ("ready to roll dice? (type 0 for yes, type 1 for no): ");
        scanf ("%d", &ready_dice);
    }
    
    /* After rolling the dice the player will be able to see what they rolled and
     the cards they got from that roll.
     */
    Game game; int roll = 0;
    if (ready_dice == 0) {
        roll = game.RollDice();
        
        //gets resource associated with number rolled
        //checks to see if resource is next to your settlements
        //adds resource card to inventory if match with dice rolled
        for (int i = 0; i < _size; i++) {
            for (int j = 0; j < _size; j++) {
                if (roll == b.GetTile(i,j).getRollVal()) {
                    if (b.GetTile(i+1, j+1).getIcon() == "$ ") {
                        if (b.GetTile(i, j).getIcon() == "W") {
                            p.inventory_[0]++;
                        } else if (b.GetTile(i, j).getIcon() == "M") {
                            p.inventory_[1]++;
                        } else if (b.GetTile(i, j).getIcon() == "O") {
                            p.inventory_[2]++;
                        } else if (b.GetTile(i, j).getIcon() == "L") {
                            p.inventory_[3]++;
                        }
                    }
                    if (b.GetTile(i+1, j-1).getIcon() == "$ ") {
                        if (b.GetTile(i, j).getIcon() == "W") {
                            p.inventory_[0]++;
                        } else if (b.GetTile(i, j).getIcon() == "M") {
                            p.inventory_[1]++;
                        } else if (b.GetTile(i, j).getIcon() == "O") {
                            p.inventory_[2]++;
                        } else if (b.GetTile(i, j).getIcon() == "L") {
                            p.inventory_[3]++;
                        }
                    }
                    if (b.GetTile(i-1, j+1).getIcon() == "$ ") {
                        if (b.GetTile(i, j).getIcon() == "W") {
                            p.inventory_[0]++;
                        } else if (b.GetTile(i, j).getIcon() == "M") {
                            p.inventory_[1]++;
                        } else if (b.GetTile(i, j).getIcon() == "O") {
                            p.inventory_[2]++;
                        } else if (b.GetTile(i, j).getIcon() == "L") {
                            p.inventory_[3]++;
                        }
                    }
                    if (b.GetTile(i-1, j-1).getIcon() == "$ ") {
                        if (b.GetTile(i, j).getIcon() == "W") {
                            p.inventory_[0]++;
                        } else if (b.GetTile(i, j).getIcon() == "M") {
                            p.inventory_[1]++;
                        } else if (b.GetTile(i, j).getIcon() == "O") {
                            p.inventory_[2]++;
                        } else if (b.GetTile(i, j).getIcon() == "L") {
                            p.inventory_[3]++;
                        }
                    }
                }
            }
        }
        
        printf ("\nYou rolled a: ");
        std::cout << roll << ' ';
    }
    
    printf ("\nYour points: ");
    std::cout << p.Points() << '\n';
    printf ("Your cards: ");
    for (int inv = 0; inv < 4; inv++) {
        std::cout << elements[inv] << ": " << p.inventory_[inv] << ", ";
    } std::cout << std::endl;
    
    /* After rolling the dice the player gets can access a menu that will give them choices
     to different aspects of the game.
     */
    int menu_choice;
    
    do {
        
        //Game is over at 5 points
       while (p.Points() > 4) {
            printf ("\nCongrats! Game is over!");
            printf ("\nYour points: ");
            std::cout << p.Points() << '\n';
           printf ("\n");
           return 0;
        }
        
        do {
        
        //Prompts the user to choose the task they want to do
        printf ("\n__________________________________________________________________________");
        printf ("\n\nWould you like to... \nbuild a road (type 0) \nbuild a settlement (type 1) \ntrade with the bank (type 2) \nreview rules (type 3) \ncontinue (type 4) \nend game (type 5)?");
        scanf ("%d", &menu_choice);
         
            //will only allow you to build a road if you have enough resources required
        if (menu_choice == 0 && p.inventory_[1] > 0 && p.inventory_[2] > 0) {
            printf ("\n\nSelect your new road:\n");
            std::cout << b.GetBoard() << '\n';
            
            //lets user select location
            printf ("\n\nEnter a row coordinate: ");
            scanf ("%d", &row_coord);
            printf ("\nEnter a column coordinate: ");
            scanf ("%d", &col_coord);
            
            while (b.GetTile(col_coord, row_coord).getIcon() != "| " & b.GetTile(col_coord, row_coord).getIcon() != "\u2014\u2014\u2014 ") {
                printf ("\nNot valid. You did not choose a road choose again.");
                printf ("\n\nEnter a row coordinate: ");
                scanf ("%d", &row_coord);
                printf ("\nEnter a column coordinate: ");
                scanf ("%d", &col_coord);
            }
            
            if (b.GetTile(col_coord, row_coord).getIcon() == "| ") {
                // make sure tiles adjacent to player towns
                test=b.GetTile(col_coord, row_coord);
                test.setIcon("! ");
                b.SetTile(col_coord, row_coord, test);
                b.MakeBoard();
                p.RIncrement();
                printf ("\n");
                std::cout << b.GetBoard() << '\n';
                printf ("\n\nYour Points:");
                std::cout << p.Points() << '\n';
                p.inventory_[1] -= 1;
                p.inventory_[2] -= 1;
                printf ("Your cards: ");
                for (int inv = 0; inv < 4; inv++) {
                    std::cout << elements[inv] << ": " << p.inventory_[inv] << ", ";
                } std::cout << std::endl;
            }
            else if (b.GetTile(col_coord, row_coord).getIcon() == "\u2014\u2014\u2014 ") {
                test=b.GetTile(col_coord, row_coord);
                test.setIcon("._. ");
                b.SetTile(col_coord, row_coord, test);
                b.MakeBoard();
                p.RIncrement();
                printf ("\n");
                std::cout << b.GetBoard() << '\n';
                printf ("\n\nYour Points:");
                std::cout << p.Points() << '\n';
                p.inventory_[1] -= 1;
                p.inventory_[2] -= 1;
                printf ("Your cards: ");
                for (int inv = 0; inv < 4; inv++) {
                    std::cout << elements[inv] << ": " << p.inventory_[inv] << ", ";
                } std::cout << std::endl;
            }
            
        }
            //will only allow you to build a settlement if you have enough resources required
        if (menu_choice == 1 && p.inventory_[0] >= 0 && p.inventory_[1] > 0 && p.inventory_[2] > 0 && p.inventory_[3] >0 ) {
            printf ("\n\nSelect your new settlement:\n");
            std::cout << b.GetBoard() << '\n';
            
            //lets user select location
            printf ("\n\nEnter a row coordinate: ");
            scanf ("%d", &row_coord);
            printf ("\nEnter a column coordinate: ");
            scanf ("%d", &col_coord);
            
            while (b.GetTile(col_coord, row_coord).getIcon() != "# ") {
                printf ("\nNot valid. You did not choose a settlement choose again.");
                printf ("\n\nEnter a row coordinate: ");
                scanf ("%d", &row_coord);
                printf ("\nEnter a column coordinate: ");
                scanf ("%d", &col_coord);
            }
            
            if (b.GetTile(col_coord, row_coord).getIcon() == "# ") {
                test=b.GetTile(col_coord, row_coord);
                test.setIcon("$ ");
                b.SetTile(col_coord, row_coord, test);
                b.MakeBoard();
                p.TIncrement();
                printf ("\n\n");
                std::cout << b.GetBoard() << '\n';
                printf ("\nYour Points: ");
                std::cout << p.Points() << '\n';
                p.inventory_[0] -= 1;
                p.inventory_[1] -= 1;
                p.inventory_[2] -= 1;
                p.inventory_[3] -= 1;
                printf ("\nThese are your cards: ");
                for (int inv = 0; inv < 4; inv++) {
                    std::cout << elements[inv] << ": " << p.inventory_[inv] << ", ";
                } std::cout << std::endl;
            }
        }
        
        //When the user selects 2 they open the bank
        if (menu_choice == 2) {
            printf ("\n__________________________________________________________________________");
            printf ("\n\nWelcome to the Bank!");
            printf ("\nYour cards: ");
            for (int inv = 0; inv < 4; inv++) {
                std::cout << elements[inv] << ": " << p.inventory_[inv] << ", ";
            } std::cout << std::endl;
            // goes to bank.cpp
            bank.Trade(&p);
            
        }
        
        //prints off rules for user if user selects 3
        if (menu_choice == 3) {
            std::cout << "\nTry to get to 5 points to win the game!\n";
            std::cout << "Open settlements locations are indicated with: #\n";
            std::cout << "Your settlements are indicated with: $\n";
            std::cout << "Open road locations are indicated with: |\n";
            std::cout << "Your roads are indicated with: ! or ._.\n";
            std::cout << "Build a road = 1 point\nBuild a settlement = 2 points\n\n";
            std::cout << "To build a road you need: 1 oil and 1 metal\n";
            std::cout << "To build a settlement you need: 1 oil, 1 metal, 1 water, 1 livestock\n\n";
        }
            
        //Continues the game if 4 is selected in the menu
        if (menu_choice == 4) {
            int ready_dice;
            printf ("\nready to roll dice? (type 0 for yes, type 1 for no): ");
            scanf ("%d", &ready_dice);
            
            while (ready_dice > 0) {
                printf ("\nready to roll dice? (type 0 for yes, type 1 for no): ");
                scanf ("%d", &ready_dice);
            }
            if (ready_dice == 0) {
                roll = game.RollDice();
                printf ("\nYou rolled a: ");
                std::cout << roll << ' ';
            }

            //gets resource associated with number rolled
            //checks to see if resource is next to your settlements
            //adds resource card to inventory if match with dice rolled
            for (int i = 0; i < _size; i++) {
                for (int j = 0; j < _size; j++) {
                    if (roll == b.GetTile(i,j).getRollVal()) {
                    if (b.GetTile(i+1, j+1).getIcon() == "$ ") {
                        if (b.GetTile(i, j).getIcon() == "W") {
                            p.inventory_[0]++;
                        } else if (b.GetTile(i, j).getIcon() == "M") {
                            p.inventory_[1]++;
                        } else if (b.GetTile(i, j).getIcon() == "O") {
                            p.inventory_[2]++;
                        } else if (b.GetTile(i, j).getIcon() == "L") {
                            p.inventory_[3]++;
                        }
                    }
                    if (b.GetTile(i+1, j-1).getIcon() == "$ ") {
                        if (b.GetTile(i, j).getIcon() == "W") {
                            p.inventory_[0]++;
                        } else if (b.GetTile(i, j).getIcon() == "M") {
                            p.inventory_[1]++;
                        } else if (b.GetTile(i, j).getIcon() == "O") {
                            p.inventory_[2]++;
                        } else if (b.GetTile(i, j).getIcon() == "L") {
                            p.inventory_[3]++;
                        }
                    }
                    if (b.GetTile(i-1, j+1).getIcon() == "$ ") {
                        if (b.GetTile(i, j).getIcon() == "W") {
                            p.inventory_[0]++;
                        } else if (b.GetTile(i, j).getIcon() == "M") {
                            p.inventory_[1]++;
                        } else if (b.GetTile(i, j).getIcon() == "O") {
                            p.inventory_[2]++;
                        } else if (b.GetTile(i, j).getIcon() == "L") {
                            p.inventory_[3]++;
                        }
                    }
                    if (b.GetTile(i-1, j-1).getIcon() == "$ ") {
                        if (b.GetTile(i, j).getIcon() == "W") {
                            p.inventory_[0]++;
                        } else if (b.GetTile(i, j).getIcon() == "M") {
                            p.inventory_[1]++;
                        } else if (b.GetTile(i, j).getIcon() == "O") {
                            p.inventory_[2]++;
                        } else if (b.GetTile(i, j).getIcon() == "L") {
                            p.inventory_[3]++;
                        }
                    }
                    }
                }
            }

            printf ("\n\n");
            std::cout << b.GetBoard() << '\n';
            printf ("\nYour Points:");
            std::cout << p.Points() << '\n';
            printf ("Your cards: ");
            for (int inv = 0; inv < 4; inv++) {
                std::cout << elements[inv] << ": " << p.inventory_[inv] << ", ";
            } std::cout << std::endl;
        }
            if (menu_choice == 5) {
                printf ("\nGame ended.\n");
                return 0;
            }
            
        } while (p.Points() < 5);
        
    } while (begin_game < 5);
    
    
    return 0;
}
