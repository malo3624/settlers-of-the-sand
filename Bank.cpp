//Bank.cpp
#include <iostream>
#include "Bank.h"

Bank::Bank()
{
    //number of resources of each type
	for (int i = 0; i < 4; i++) {
		inventory_[i] = 999;
	}
}

std::string elements[4] = {"Water", "Metal", "Oil", "Livestock"};

//Trade menu choice (2)
void Bank::Trade(Player* p){
	int sell_opt = 0; int buy_opt = 0; char yn_opt = ' ';
	std::string options[4] = { "Water", "Metal", "Oil", "Livestock" };

    int bank_menu2;
    printf ("\n Do you want to trade or exit? (type 0 to trade or 1 to exit)");
    scanf ("%d", &bank_menu2);
    
    //asks the user what they would like to trade and what they would like to trade for
    while (bank_menu2 == 0) {
        if (((sell_opt < 4) && (sell_opt >= 0)) && ((buy_opt < 4) && (buy_opt >= 0))) {
            std::cout << "\nWhat do you want to sell? (Type 0 for Water, 1 for Metal, 2 for Oil, 3 for Livestock)" << std::endl;
            std::cin >> sell_opt;
            std::cout << "\nWhat would you like to trade for? (Type 0 for Water, 1 for Metal, 2 for Oil, 3 for Livestock)" << std::endl;
            std::cin >> buy_opt;

            again:
            std::cout << "\nPrice of 1 " << options[buy_opt] << " is 2 " << options[sell_opt] << ". Accept terms? [Y/N]" << std::endl;
            std::cin >> yn_opt;

                if ((yn_opt == 'Y') || (yn_opt == 'y')) {
                    std::cout << "\nExcellent. Commencing trade...";
                    
                    //give two cards to bank get back one
                    p->inventory_[sell_opt] -= 2;
                    p->inventory_[buy_opt] += 1;

                    std::cout << "Done! Come again soon!" << std::endl;
                    
                    //updated cards after trade
                    printf ("\nThese are your cards: ");
                    for (int inv = 0; inv < 4; inv++) {
                        std::cout << elements[inv] << ": " << p->inventory_[inv] << ", ";
                    } std::cout << std::endl;
                    return;
                }
                else if ((yn_opt == 'N') || (yn_opt == 'n')) {
                    std::cout << "Too bad, then. Come again soon!" << std::endl;
                }
                else {
                    std::cout << "Invalid response. Try again." << std::endl;
                    goto again;
                }
        }
    }
}
