//Game.h
#ifndef _GAME_H_
#define _GAME_H_
#include "Interactor.h"
#include "Board.h"
#include "Bank.h"
#include <iostream>
#include <string>

// Trading, taking turns, maps and item placements;
// Dice rolling, setting up player classes.

class Game
{
public:
    Game();
	int RollDice();

private:
	;
};

#endif
