//Board.h

#ifndef _BOARD_H_
#define _BOARD_H_

#include <iostream>
#include <fstream>
#include <locale>
#include <string.h>

//defines tile types
enum class TileType {Water, Metal, Oil, Livestock, Road, Town};

class Tile {
    
public:
    Tile() {};
    Tile(TileType type_)
    {
        Ttype_ = type_;
    }
    std::string GetType() {return Stringify(Ttype_);}
    std::string Stringify(TileType);
    
    //get and set roll value for tiles
    void setRollVal(int v) { resource_roll_value = v; }
    int getRollVal() { return resource_roll_value; }
    
    //set and get icon of resouces roads and towns
    void setIcon(const std::string& inp) { icon_ = inp; }
    std::string getIcon() { return icon_; }
    
private:
    TileType Ttype_;
    int resource_roll_value;
    std::string icon_;
};

class Board {

public:
    Board(int);
    
    //gets specific tile and sets tile
    Tile GetTile(int i, int j) { return board_[i][j]; }
    void SetTile(int i, int j, Tile ic) { board_[i][j] = ic; }

    std::string GetBoard() const { return print_board_; }
    void MakeBoard();
    
private:
    // 9x9 board 
	Tile board_[9][9];
    std::string print_board_;
    int resource_chance;
    int board_size;
};

std::ostream& operator<< (std::ostream &out, const Board &b);
#endif
