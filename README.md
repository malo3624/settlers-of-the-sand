By: Connor Dowd, Victoria Velasquez, Matthew Louchart

Build this project by using a complier application.

Import the project into a complier application.
Select the build button, and then select the run button.

We have been having trouble as a team working in the command line and it does not work for some people.
We made the collective decsion to use a complier applicattion such as XCode to work on the project.

How to run on terminal:
command : g++ -std=c++14 main.cpp Game.cpp Board.cpp Bank.cpp Interactor.cpp -o settlers && ./settlers

This will compile and run the game properly.
