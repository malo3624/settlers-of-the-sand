#include "Board.h"
#include "Bank.h"
#include "Game.h"
#include "Interactor.h"
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
// Unit testing scenarios for various classes and functions: 
//    make sure everything works properly

// Tests for Board class

TEST_CASE("Tile properties function.", "[tile]") {
	Tile t = TileType::Water;
	
	t.setIcon(":)");
	REQUIRE(t.getIcon() == ":)");

	t.setRollVal(10);
	REQUIRE(t.getRollVal() == 10);
}

TEST_CASE("Board properties function as intended.", "[board]") {
	Board b(5);

	REQUIRE(b.GetTile(0,0).getIcon() == "# ");
	REQUIRE(b.GetTile(4,4).getIcon() == "# ");
	REQUIRE(b.GetTile(1,0).getIcon() == "\u2014\u2014\u2014 ");
	REQUIRE(b.GetTile(0,1).getIcon() == "| ");

	REQUIRE(b.GetTile(0,0).getRollVal() == 0);

	Tile test = b.GetTile(2,2);
	test.setIcon("& ");
	b.SetTile(2,2,test);
	REQUIRE(b.GetTile(2,2).getIcon() == "& ");
}

// Tests for Bank class
TEST_CASE("Bank class functions work as intended.","[bank]") {
	// The only function Bank has is Trade(Player*), which
	// requires human input that insofar we cannot replicate 
	// on catch.
	REQUIRE(true);
}

// Tests for Game class
// The only game class function used is RollDice, a random and 
// therefore inherently untestable function.

// Tests for Iterator class and inherited classes
TEST_CASE("Interactor class (and derived class) functions work.","[interactors]") {
	// Mostly testing player class, as virtually all interactions are with just player
	// No interactions with separate parent (Interactor) class instance.
	Player hater;

	hater.RIncrement();
	hater.RIncrement();
	hater.TIncrement();
	REQUIRE(hater.Points() == 4);
	REQUIRE(hater.GetNumRoads() == 2);
	REQUIRE(hater.GetNumTowns() == 1);
}