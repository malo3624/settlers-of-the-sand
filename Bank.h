//Bank.h
#ifndef _BANK_H_
#define _BANK_H_

#include "Interactor.h"
#include <vector>

class Bank {


public:
    Bank();
	void Trade(Player* p);

private:
	int inventory_[4];
};
#endif
