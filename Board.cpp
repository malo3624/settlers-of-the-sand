//Board.cpp

#include "Board.h"

//Type of tile: water, metal, oil, livestock, road, or town
std::string Tile::Stringify(TileType type_){
    if(type_ == TileType::Water) {
        return "Water";
    }else if(type_ == TileType::Metal){
        return "Metal";
    }else if(type_ == TileType::Oil){
        return "Oil";
    }else if(type_ == TileType::Livestock){
        return "Livestock";
    }else if(type_ == TileType::Road){
        return "Road";
    }else if(type_ == TileType::Town){
        return "Town";
    }
    return 0;
}

Board::Board(const int _size){
    
    
    board_size = _size;

    for(int i = 0; i < _size; i++){
        for(int j = 0; j < _size; j++){
            resource_chance = rand()%100+1;
            
            if (i%2 == 0) {
                if (j%2 == 0) {
                    //tiles where you can place a town are #'s
                    board_[j][i] = TileType::Town;
                    board_[j][i].setIcon("# ");
                } else {
                    //tiles where you can place a horizontal road are _
                    board_[j][i] = TileType::Road;
                    board_[j][i].setIcon("\u2014\u2014\u2014 ");
                }
                board_[j][i].setRollVal(0);
            } else {
                if (j%2 == 0) {
                    //tiles where you can place a vertical road are |
                    board_[j][i] = TileType::Road;
                    board_[j][i].setRollVal(0);
                    board_[j][i].setIcon("| ");
                } else {
                    // Each resource has 25% chance of spawning
                    if (resource_chance <= 25){
                        board_[j][i] = TileType::Water;
                        board_[j][i].setIcon("W");
                    } else if ((26 <= resource_chance) && (resource_chance <= 50)){
                        board_[j][i] = TileType::Metal;
                        board_[j][i].setIcon("M");
                    } else if ((51 <= resource_chance) && (resource_chance <= 75)){
                        board_[j][i] = TileType::Oil;
                        board_[j][i].setIcon("O");
                    } else if (76 <= resource_chance){
                        board_[j][i] = TileType::Livestock;
                        board_[j][i].setIcon("L");
                    }
                    board_[j][i].setRollVal(rand()%11+2);
			}}
	}}
    MakeBoard();
}

//updates and makes board with designated tiles
void Board::MakeBoard()
{
	int _size = board_size;
    print_board_ = "";
	print_board_ += "    "; for (int j = 0; j < _size; j++) { print_board_ += std::to_string(j) + "  "; } print_board_ += "\n";
    print_board_ += "    "; for (int j = 0; j <= (2*_size)+6; j++) { print_board_ += "\u2014"; } print_board_ += "\n";
    
    for(int i = 0; i < _size; i++){
        print_board_ += std::to_string(i) + " | ";
        for(int j = 0; j < _size; j++){
            if (i%2 == 0) {
                print_board_ += board_[j][i].getIcon();
            } else {
                if (j%2 == 0) {
                    print_board_ += board_[j][i].getIcon();
                } else {
                    print_board_ += board_[j][i].getIcon() + ":" + std::to_string(board_[j][i].getRollVal());
                    if (board_[j][i].getRollVal() < 10) { print_board_ += " "; }
                }
            }
        }
        print_board_ += "\n"; //prints the board with random placements for water, metal, oil, and live stock so that
        // each game has a new map
    }
}


// Operator overload
std::ostream& operator<< (std::ostream &out, const Board &b)
{
    out << b.GetBoard();
    return out;
}
